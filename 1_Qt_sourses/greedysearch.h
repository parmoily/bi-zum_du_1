#ifndef GREEDYSEARCH_H
#define GREEDYSEARCH_H


#include "field.h"

class GreedySearch
{
public:
    GreedySearch(Field *field,
                 int start_x = 0,
                 int start_y = 0,
                 int finish_x = 0,
                 int finish_y = 0);
    void runSearch();

private:
    Field *m_field;
    int m_start_x = 0;
    int m_start_y = 0;
    int m_finish_x = 0;
    int m_finish_y = 0;
};

#endif // GREEDYSEARCH_H
