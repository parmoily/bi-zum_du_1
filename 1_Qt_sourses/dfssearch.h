#ifndef DFSSEARCH_H
#define DFSSEARCH_H

#include "field.h"



class DFSSearch
{
public:
    DFSSearch(Field *field,
              int start_x = 0,
              int start_y = 0,
              int finish_x = 0,
              int finish_y = 0);
    void runSearch();
    void req(QList<Point *> buffer);
    int nodesExpanded = 0;
    Point * finishPoint;

private:
    Field *m_field;
    int m_start_x = 0;
    int m_start_y = 0;
    int m_finish_x = 0;
    int m_finish_y = 0;
};

#endif // DFSSEARCH_H
