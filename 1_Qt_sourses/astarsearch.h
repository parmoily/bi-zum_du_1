#ifndef ASTARSEARCH_H
#define ASTARSEARCH_H


#include "field.h"

#define MAX_INT 1000000

class AStarSearch
{
public:
    AStarSearch(Field *field,
                int start_x = 0,
                int start_y = 0,
                int finish_x = 0,
                int finish_y = 0);
   void runSearch();

private:
    Field *m_field;
    int m_start_x = 0;
    int m_start_y = 0;
    int m_finish_x = 0;
    int m_finish_y = 0;
};

#endif // ASTARSEARCH_H
