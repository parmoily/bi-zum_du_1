#include "bfssearch.h"

BFSSearch::BFSSearch(Field *field, int start_x, int start_y, int finish_x, int finish_y)
{
    m_field = field;
     m_start_x = start_x;
     m_start_y = start_y;
     m_finish_x = finish_x;
     m_finish_y = finish_y;

     m_field->at(m_start_x, m_start_y)->setType('S');
     m_field->at(finish_x, m_finish_y)->setType('E');
}

void BFSSearch::runSearch()
{
    Point * startPoint = m_field->at(m_start_x, m_start_y);
    Point* finishPoint = m_field->at(m_finish_x, m_finish_y);

    int nodesExpanded = 0;
    int pathLen = 0;

    QList<Point *> buffer;
    QList<Point *> bufferTMP;
    QList<Point *> bufferTMP2;
    QList<Point *> bufferNeighbors;
    buffer.append(startPoint);
    startPoint->open();
    nodesExpanded++;

    while(startPoint != finishPoint)
    {
        pathLen++;

        for(auto it : buffer)
        {
            bufferNeighbors = m_field->getNeighbors(it);
            for(auto tmpIt : bufferNeighbors)
            {
                if( !tmpIt->m_opened && !tmpIt->m_closed)
                {
                    bufferTMP2.append(tmpIt);
                    tmpIt->parent = it;
                }
            }

            for(auto tmpIt : bufferTMP2)
            {
                tmpIt->open();
            }
            nodesExpanded+=bufferTMP2.size();

            bufferTMP.append(bufferTMP2);
            bufferNeighbors.clear();
            bufferTMP2.clear();
        }


        for(auto it : buffer)
            it->close();
        buffer.clear();
        buffer.append(bufferTMP);
        for(auto it : buffer)
        {
            if(it == finishPoint)
              startPoint = it;
        }

        bufferTMP.clear();
        m_field->printField();
        qDebug()<<"";
        qDebug()<<"--------------------------------------------";
        qDebug()<<"";
        qDebug()<<"Nodes expanded: "<<nodesExpanded;
        qDebug()<<"Path length: "<<pathLen;
        qDebug()<<"";

    }

    m_field->writePath(finishPoint);
    m_field->printField();
    qDebug()<<"";
    qDebug()<<"--------------------------------------------";
    qDebug()<<"";
    qDebug()<<"Nodes expanded: "<<nodesExpanded;
    qDebug()<<"Path length: "<<m_field->path;
}
