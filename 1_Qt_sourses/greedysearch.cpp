#include "greedysearch.h"


GreedySearch::GreedySearch(Field *field, int start_x, int start_y, int finish_x, int finish_y)
{
    m_field = field;
     m_start_x = start_x;
     m_start_y = start_y;
     m_finish_x = finish_x;
     m_finish_y = finish_y;

     m_field->at(m_start_x, m_start_y)->setType('S');
     m_field->at(finish_x, m_finish_y)->setType('E');
}

void GreedySearch::runSearch()
{
    Point * startPoint = m_field->at(m_start_x, m_start_y);
    Point* finishPoint = m_field->at(m_finish_x, m_finish_y);

    int nodesExpanded = 0;
    QList<Point *> buffer;
    QList<Point *> bufferTMP;
    QList<Point *> bufferNeighbors;

    buffer.append(startPoint);
    startPoint->open();
    nodesExpanded++;

    while(startPoint != finishPoint)
    {
        bufferNeighbors = m_field->getNeighbors(startPoint);
        for(auto it : bufferNeighbors)
        {
            if( !it->m_opened && !it->m_closed)
            {
                it->open();
                it->parent = startPoint;
                bufferTMP.append(it);
                nodesExpanded++;
            }
        }

        startPoint->close();
        buffer.removeOne(startPoint);
        buffer.append(bufferTMP);
        bufferTMP.clear();
        bufferNeighbors.clear();

        int minHeuristic = 1000000; //MAX_INT

        for(auto it : buffer)
        {
            if(it->m_heuristic < minHeuristic){
                startPoint = it;
                minHeuristic = it->m_heuristic;
            }
        }

        for(auto it : buffer)
        {
            if(it == finishPoint)
              startPoint = it;
        }

        m_field->printField();
        qDebug()<<"";
        qDebug()<<"--------------------------------------------";
        qDebug()<<"";
        qDebug()<<"Nodes expanded: "<<nodesExpanded;
        qDebug()<<"";

    }

    m_field->writePath(finishPoint);
    m_field->printField();
    qDebug()<<"";
    qDebug()<<"--------------------------------------------";
    qDebug()<<"";
    qDebug()<<"Nodes expanded: "<<nodesExpanded;
    qDebug()<<"Path length: "<<m_field->path;


}
