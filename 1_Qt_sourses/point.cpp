#include "point.h"

Point::Point(QChar type, int x, int y)
{
    setType(type);
    m_x = x;
    m_y = y;
}

void Point::setType(QChar type)
{
    if(type == 'X')
    {
        m_type = X;
    }
    if(type == ' ')
    {
        m_type = Nothing;
    }
    if(type == '#')
    {
        m_type = Hash;
    }
    if(type == 'S')
    {
        m_type = S;
    }
    if(type == 'E')
    {
        m_type = E;
    }
    if(type == 'o')
    {
        m_type = o;
    }
    if(type == '.')
    {
        m_type = p;
    }
}

QChar Point::pointChar()
{
    if(m_type == X)
    {
        return 'X';
    }
    if(m_type == Nothing)
    {
        return ' ';
    }
    if(m_type == Hash)
    {
        return '#';
    }
    if(m_type == S)
    {
        return 'S';
    }
    if(m_type == E)
    {
        return 'E';
    }
    if(m_type == p)
    {
        return '.';
    }

    return 'o';
}

void Point::open()
{
    m_opened = true;
    m_closed = false;

    if(m_type != S && m_type != E )
         m_type = o;
}

void Point::close()
{
    m_opened = false;
    m_closed = true;
    if(m_type != S && m_type != E )
        m_type = Hash;
}

void Point::refresh()
{
    m_opened = false;
    m_closed = false;
    parent = nullptr;

    if(m_type != X)
    {
        if(m_type != Nothing)
        {
            m_type = Nothing;
        }
    }
}

