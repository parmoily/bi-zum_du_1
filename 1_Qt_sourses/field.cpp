#include "field.h"
#include <QtMath>

Field::Field(QList< QList<QChar> > field, int x, int y, QObject *parent) : QObject(parent)
{
    m_X = x;
    m_Y = y;

    for(int i = 0; i < y; i++)
    {
        QList<Point> tmp;
        for(int j = 0; j < x; j++)
        {
            tmp.append(Point(field[i][j], j, i));
        }
        m_data.append(tmp);
    }
}

Point* Field::at(int x, int y)
{
    return &( m_data[y][x] );
}

QList<Point *> Field::getNeighbors(Point *point)
{
    QList<Point *> res;

    if(point->m_y + 1 < m_Y && m_data[point->m_y + 1][point->m_x].m_type != X)
        res.append(&(m_data[point->m_y + 1][point->m_x]));
    if(point->m_x + 1 < m_X && m_data[point->m_y][point->m_x + 1].m_type != X)
        res.append(&(m_data[point->m_y][point->m_x + 1]));

    if(point->m_y  > 0 && m_data[point->m_y - 1][point->m_x].m_type != X)
        res.append(&(m_data[point->m_y - 1][point->m_x]));
    if(point->m_x - 1 < m_X && m_data[point->m_y][point->m_x - 1].m_type != X)
        res.append(&(m_data[point->m_y][point->m_x - 1]));

    return res;
}

QList<Point *> Field::getOpened()
{
    QList<Point *> res;

    for(int i = 0; i < m_Y; i++)
    {
        for(int j = 0; j < m_X; j++)
        {
            if(m_data[i][j].m_opened)
                res.append(&(m_data[i][j]));
        }
    }

    return res;
}

void Field::writePath(Point* end)
{
    if(end->m_type != E && end->m_type != S)
        end->setType('.');
    if(end->parent != nullptr){
        writePath(end->parent);
        path ++;
    }
}

void Field::refreshCalculations()
{
    path = 0;
    for(int i = 0; i < m_Y; i++)
    {
        for(int j = 0; j < m_X; j++)
        {
            m_data[i][j].refresh();
        }
    }
}

void Field::makeHeuristic(int finish_x, int finish_y)
{
    for(int i = 0; i < m_Y; i++)
    {
        for(int j = 0; j < m_X; j++)
        {
            m_data[i][j].m_heuristic = (int)(qSqrt(qPow(finish_x - j,2) + qPow(finish_y - i,2)));//Pythagoras
        }
    }
}

void Field::printField()
{
    QString tmp = "";
    for(int i = 0; i < m_Y; i++)
    {
        for(int j = 0; j < m_X; j++)
        {
            tmp.append( m_data[i][j].pointChar() );
        }
        qDebug() << tmp;
        tmp = "";
    }
    qDebug()<<"";
    qDebug()<<"--------------------------------------------";
    qDebug()<<"S Start";
    qDebug()<<"E End";
    qDebug()<<"o Opened node";
    qDebug()<<"# Closed node";
    qDebug()<<"X Wall";
    qDebug()<<"space Fresh node";
    qDebug()<<". Path";

}
