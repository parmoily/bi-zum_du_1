#ifndef POINT_H
#define POINT_H

#include <QObject>

enum Point_Types {
    X,
    Hash,
    S,
    E,
    o,
    p,
    Nothing
};

class Point
{
public:
    explicit Point(QChar type, int x, int y);
    void setType(QChar type);
    QChar pointChar();
    void open();
    void close();
    void refresh();

    bool m_opened = false;
    bool m_closed = false;
    int m_x;
    int m_y;
    int m_heuristic;
    Point* parent = nullptr;

    Point_Types m_type = Nothing;
private:

};

#endif // POINT_H
