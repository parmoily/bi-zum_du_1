#include "astarsearch.h"
#include "bfssearch.h"
#include "dfssearch.h"
#include "dijsearch.h"
#include "field.h"
#include "greedysearch.h"
#include "randomsearch.h"

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QTime>

QString startOutputName;
QString outputName;

void myMessageOutput(QtMsgType type, const QMessageLogContext &, const QString &msg)
{
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("Warning: %1").arg(msg);
    break;
    case QtCriticalMsg:
        txt = QString("Critical: %1").arg(msg);
    break;
    case QtFatalMsg:
        txt = QString("Fatal: %1").arg(msg);
    break;
    }

    QFile outFile(outputName);
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;
}

int main(int argc, char *argv[])
{

    printf("\n%s\n","Start !");

    QDateTime  currTime = QDateTime::currentDateTime();
    QString aa = currTime.toString("_mm_ss");
    startOutputName ="output" + aa + ".txt";
    outputName = startOutputName;

    qInstallMessageHandler(myMessageOutput); // Install the handler
    QCoreApplication a(argc, argv);

    QFile file("data.txt");

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        printf("\n%s\n","Wasnt opened\n");
        return 0;
    }

    QTextStream in(&file);

    QList< QList<QChar> > field;
    int x = 0;
    int y = 0;
    int start_x = 0;
    int start_y = 0;
    int finish_x = 0;
    int finish_y = 0;

    QString line = in.readLine();
    x = line.size();
    while(line[0] != 's')
    {
        y ++;

        QList<QChar> tmp;
        for(int i = 0; i < x; i++)
        {
            tmp.append(line[i]);
        }

        field.append(tmp);

        line = in.readLine();
    }

    QStringList tmp = line.split(' ');
    start_x = tmp[1].remove(tmp[1].size() - 1, 1).toInt();
    start_y = tmp[2].toInt();

    line = in.readLine();

    tmp = line.split(' ');
    finish_x = tmp[1].remove(tmp[1].size() - 1, 1).toInt();
    finish_y = tmp[2].toInt();

    Field *a_Field = new Field(field, x, y);
    a_Field->printField();
    a_Field->makeHeuristic(finish_x, finish_y);

    /// Tests ----------------------------------------------------------------------------

    outputName = "BFSSearch_" + startOutputName;
    BFSSearch bfsSearch = BFSSearch(a_Field, start_x, start_y, finish_x, finish_y );
    bfsSearch.runSearch();
    a_Field->refreshCalculations();

    outputName = "RandomSearch_" + startOutputName;
    RandomSearch randomSearch = RandomSearch(a_Field, start_x, start_y, finish_x, finish_y );
    randomSearch.runSearch();
    a_Field->refreshCalculations();

    outputName = "DFSSearch_" + startOutputName;
    DFSSearch dfsSearch = DFSSearch(a_Field, start_x, start_y, finish_x, finish_y );
    dfsSearch.runSearch();
    a_Field->refreshCalculations();


    outputName = "DijSearch_" + startOutputName;
    DijSearch dijSearch = DijSearch(a_Field, start_x, start_y, finish_x, finish_y );
    dijSearch.runSearch();
    a_Field->refreshCalculations();

    outputName = "GreedySearch_" + startOutputName;
    GreedySearch greedySearch = GreedySearch(a_Field, start_x, start_y, finish_x, finish_y );
    greedySearch.runSearch();
    a_Field->refreshCalculations();

    outputName = "AStarSearch_" + startOutputName;
    AStarSearch sStarSearch = AStarSearch(a_Field, start_x, start_y, finish_x, finish_y );
    sStarSearch.runSearch();
    a_Field->refreshCalculations();

    printf("\n%s\n","End !");

    return a.exec();
}
