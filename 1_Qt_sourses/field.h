#ifndef FIELD_H
#define FIELD_H

#include "point.h"

#include <QObject>
#include <QDebug>

class Field : public QObject
{
    Q_OBJECT
public:
    explicit Field(QList< QList<QChar> > field, int x, int y, QObject *parent = nullptr);
    Point* at(int x, int y);
    QList<Point *> getNeighbors(Point * point);
    QList<Point *> getOpened();
    void writePath(Point* end);
    void refreshCalculations();

    void makeHeuristic(int finish_x, int finish_y);

    int m_X;
    int m_Y;
    int path = 0;

    void printField();
private:

    QList< QList<Point> > m_data;

signals:

};

#endif // FIELD_H
