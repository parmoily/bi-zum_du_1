#include "dfssearch.h"

DFSSearch::DFSSearch(Field *field, int start_x, int start_y, int finish_x, int finish_y)
{
     m_field = field;
     m_start_x = start_x;
     m_start_y = start_y;
     m_finish_x = finish_x;
     m_finish_y = finish_y;

     m_field->at(m_start_x, m_start_y)->setType('S');
     m_field->at(finish_x, m_finish_y)->setType('E');
}

void DFSSearch::runSearch()
{
    Point * startPoint = m_field->at(m_start_x, m_start_y);
    finishPoint = m_field->at(m_finish_x, m_finish_y);
    QList<Point *> buffer;
    buffer.append(startPoint);
    startPoint->open();
    nodesExpanded++;

    req(buffer);

    m_field->writePath(finishPoint);
    m_field->printField();
    qDebug()<<"Path length: "<<m_field->path;
}

void DFSSearch::req(QList<Point *> buffer)
{
    QList<Point *> bufferNeighbors;
    QList<Point *> bufferTMP;
    bufferNeighbors = m_field->getNeighbors(buffer[0]);
    buffer[0]->close();
    for(auto it : bufferNeighbors)
    {
        if( !it->m_opened && !it->m_closed)
        {
            it->open();
            it->parent = buffer[0];
            bufferTMP.append(it);
            nodesExpanded++;
        }
    }
    buffer.removeAt(0);
    buffer.append(bufferTMP);

    m_field->printField();
    qDebug()<<"";
    qDebug()<<"--------------------------------------------";
    qDebug()<<"";
    qDebug()<<"Nodes expanded: "<<nodesExpanded;
    qDebug()<<"Path length: "<<m_field->path;
    qDebug()<<"";

    for(auto it : bufferTMP)
    {
        if(it == finishPoint)
          return;
    }
    req(buffer);
}
