#include "dijsearch.h"

DijSearch::DijSearch(Field *field, int start_x, int start_y, int finish_x, int finish_y)
{
    m_field = field;
    m_start_x = start_x;
    m_start_y = start_y;
    m_finish_x = finish_x;
    m_finish_y = finish_y;

    m_field->at(m_start_x, m_start_y)->setType('S');
    m_field->at(finish_x, m_finish_y)->setType('E');
}

void DijSearch::runSearch()
{
    Point * startPoint = m_field->at(m_start_x, m_start_y);
    Point * finishPoint = m_field->at(m_finish_x, m_finish_y);
    int d[m_field->m_Y][m_field->m_X];//min distances
//    QList<QList<QList<Point *> >> p; //path
    int possibleToVisit = 0;
    int visited = 0;
    for(int i = 0; i < m_field->m_Y; i++)
    {
        for(int j = 0; j < m_field->m_X; j++)
        {
            if(m_field->at(j, i)->m_type != X)
                possibleToVisit ++;
            d[i][j] = MAX_INT;
        }
    }
    d[m_start_y][m_start_x] = 0;

    while(visited != possibleToVisit)
    {
        int minDist = MAX_INT;
        int minX = 0;
        int minY = 0;

        ///Finfing min distanse
        for(int i = 0; i < m_field->m_Y; i++)
        {
            for(int j = 0; j < m_field->m_X; j++)
            {
                if(m_field->at(j, i)->m_closed != true
                        && minDist > d[i][j]){
                    minY = i;
                    minX = j;
                    minDist = d[i][j];
                }
            }
        }

        if(m_field->at(minX, minY) == finishPoint)
            break;

        m_field->at(minX, minY)->close();
        visited++;


        QList<Point *> bufferNeighbors;
        bufferNeighbors = m_field->getNeighbors(m_field->at(minX, minY));

        for(auto it : bufferNeighbors )
        {
            if(d[it->m_y][it->m_x] > d[minY][minX] + 1)
            {
                d[it->m_y][it->m_x] = d[minY][minX] + 1;
                m_field->at(it->m_x, it->m_y)->parent = m_field->at(minX, minY);
            }
        }

        m_field->printField();
        qDebug()<<"";
        qDebug()<<"--------------------------------------------";
        qDebug()<<"";
        qDebug()<<"Nodes expanded: "<<visited;
        qDebug()<<"";

    }



    m_field->writePath(finishPoint);
    m_field->printField();
    qDebug()<<"";
    qDebug()<<"--------------------------------------------";
    qDebug()<<"";
    qDebug()<<"Nodes expanded: "<<visited;
    qDebug()<<"Path length: "<<m_field->path;
}
